﻿namespace CluedIn.CodingChallenge.Src.Services
{
    using System.Linq;
    using CluedIn.CodingChallenge.Src.PubSub.Publishers;

    public class CoreService : ICoreService
    {
        private readonly IPublisher publisher;

        public CoreService(IPublisher publisher)
        {
            this.publisher = publisher;
        }

        public void Execute(string message)
        {
            var messageReversed = string.Join(null, message.Reverse());

            this.publisher.Publish(messageReversed);
        }
    }
}
