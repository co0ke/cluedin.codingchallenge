﻿namespace CluedIn.CodingChallenge.Src.Services
{
    public interface ICoreService
    {
        void Execute(string name);
    }
}