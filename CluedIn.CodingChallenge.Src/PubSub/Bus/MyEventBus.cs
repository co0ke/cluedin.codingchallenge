﻿namespace CluedIn.CodingChallenge.Src.PubSub.Bus
{
    using CluedIn.CodingChallenge.Src.Logging;
    using CluedIn.CodingChallenge.Src.PubSub.Subscribers;
    using Redbus;
    using Redbus.Events;
    using Redbus.Interfaces;

    public class MyEventBus : IMyEventBus
    {
        private readonly IEventBus redBus;

        public MyEventBus(IEventBus redBus)
        {
            this.redBus = redBus;

            var subscriberOne = new SubscriberOne(new Logger());
            var subscriberTwo = new SubscriberTwo(new Logger());

            this.Subscribe<PayloadEvent<string>>(subscriberOne);
            this.Subscribe<PayloadEvent<string>>(subscriberTwo);
        }

        public SubscriptionToken Subscribe<T>(ISubscriber subscriber) where T : PayloadEvent<string>
        {
            return this.redBus.Subscribe<T>(subscriber.Handle);
        }

        public void PublishAsync<T>(string message) where T : PayloadEvent<string>
        {
            redBus.PublishAsync(new PayloadEvent<string>(message));
        }
    }
}