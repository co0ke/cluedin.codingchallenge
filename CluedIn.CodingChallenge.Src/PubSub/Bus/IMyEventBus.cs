﻿namespace CluedIn.CodingChallenge.Src.PubSub.Bus
{
    using CluedIn.CodingChallenge.Src.PubSub.Subscribers;
    using Redbus;
    using Redbus.Events;

    public interface IMyEventBus
    {
        SubscriptionToken Subscribe<T>(ISubscriber subscriber) where T : PayloadEvent<string>;

        void PublishAsync<T>(string message) where T : PayloadEvent<string>;
    }
}
