﻿namespace CluedIn.CodingChallenge.Src.PubSub.Subscribers
{
    using CluedIn.CodingChallenge.Src.Logging;
    using Redbus.Events;

    public class SubscriberTwo : ISubscriber
    {
        private readonly ILogger logger;

        public SubscriberTwo(ILogger logger)
        {
            this.logger = logger;
        }

        public void Handle(PayloadEvent<string> @event)
        {
            this.logger.Log($"SubscriberTwo: {@event.Payload}");
        }
    }
}