﻿namespace CluedIn.CodingChallenge.Src.PubSub.Subscribers
{
    using CluedIn.CodingChallenge.Src.Logging;
    using Redbus.Events;

    public class SubscriberOne : ISubscriber
    {
        private readonly ILogger logger;

        public SubscriberOne(ILogger logger)
        {
            this.logger = logger;
        }

        public void Handle(PayloadEvent<string> @event)
        {
            this.logger.Log($"SubscriberOne: {@event.Payload}");
        }
    }
}