﻿namespace CluedIn.CodingChallenge.Src.PubSub.Subscribers
{
    using Redbus.Events;

    public interface ISubscriber
    {
        void Handle(PayloadEvent<string> @event);
    }
}
