﻿namespace CluedIn.CodingChallenge.Src.PubSub.Publishers
{
    using CluedIn.CodingChallenge.Src.PubSub.Bus;
    using Redbus.Events;

    public class Publisher : IPublisher
    {
        private readonly IMyEventBus eventBus;

        public Publisher(IMyEventBus eventBus)
        {
            this.eventBus = eventBus;
        }

        public void Publish(string message)
        {
            this.eventBus.PublishAsync<PayloadEvent<string>>(message);
        }
    }
}