﻿namespace CluedIn.CodingChallenge.Src.PubSub.Publishers
{
    public interface IPublisher
    {
        void Publish(string message);
    }
}