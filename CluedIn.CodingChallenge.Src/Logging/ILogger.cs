﻿namespace CluedIn.CodingChallenge.Src.Logging
{
    public interface ILogger
    {
        void Log(string message);
    }
}
