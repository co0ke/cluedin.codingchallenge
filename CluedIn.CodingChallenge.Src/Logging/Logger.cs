﻿namespace CluedIn.CodingChallenge.Src.Logging
{
    using System;

    public class Logger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
