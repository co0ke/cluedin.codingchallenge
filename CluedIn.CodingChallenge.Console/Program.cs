﻿namespace CluedIn.CodingChallenge.Console
{
    using System;
    using System.Threading;
    using CluedIn.CodingChallenge.Src.PubSub.Bus;
    using CluedIn.CodingChallenge.Src.PubSub.Publishers;
    using CluedIn.CodingChallenge.Src.Services;
    using Redbus;

    class Program
    {
        static void Main(string[] args)
        {
            var service = GetService();

            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();

            service.Execute(name);

            Thread.Sleep(1000);
            Console.WriteLine("Press enter key to exit...");
            Console.ReadLine();
        }

        private static ICoreService GetService()
        {
            var eventBus = new MyEventBus(new EventBus());
            var publisher = new Publisher(eventBus);
            var service = new CoreService(publisher);

            return service;
        }
    }
}
