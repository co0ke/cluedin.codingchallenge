# CluedIn Coding Challenge #

Includes console app with unit tests to demonstrate pub/sub using [RedBus](https://github.com/mxjones/RedBus).

You should (hopefully) just be able to load up VS, set CluedIn.CodingChallenge.Console as the startup project, and hit play.

The flow is:

* You enter your name
* Name is reversed
* An event is published containing the reversed name
* Subscribers log out the reversed name

### TODO ###

* Topics
* Message persistence.
* Dependency injection.
* Consider location of subscriber registration. Currently the bus is responsible, but this looks like a violation of OCP.
* Remove restriction on type of events that can be published/subscribed to.
* Integration tests.
* Add folder hierarchy to UnitTests project to mimic Src project.


