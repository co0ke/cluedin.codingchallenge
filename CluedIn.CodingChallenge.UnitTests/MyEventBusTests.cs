namespace CluedIn.CodingChallenge.UnitTests
{
    using System;
    using CluedIn.CodingChallenge.Src.PubSub.Bus;
    using Moq;
    using Redbus.Events;
    using Redbus.Interfaces;
    using Xunit;

    public class MyEventBusTests
    {
        [Fact]
        public void Constructor_WhenCalled_PerformsAppropriateSetup()
        {
            // Arrange
            var redBus = new Mock<IEventBus>();

            // Act
            var myEventBus = new MyEventBus(redBus.Object);

            // Assert
            redBus.Verify(x => x.Subscribe(It.IsAny<Action<PayloadEvent<string>>>()), Times.Exactly(2));
        }
    }
}
