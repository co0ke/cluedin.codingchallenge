namespace CluedIn.CodingChallenge.UnitTests
{
    using CluedIn.CodingChallenge.Src.PubSub.Bus;
    using CluedIn.CodingChallenge.Src.PubSub.Publishers;
    using Moq;
    using Redbus.Events;
    using Xunit;

    public class PublisherUnitTests
    {
        [Fact]
        public void Execute_WhenCalled_PublishesExpectedEvent()
        {
            // Arrange
            var eventBus = new Mock<IMyEventBus>();
            var coreService = new Publisher(eventBus.Object);
            var message = "123";

            // Act
            coreService.Publish(message);

            // Assert
            eventBus.Verify(x => x.PublishAsync<PayloadEvent<string>>(message), Times.Once);
            eventBus.VerifyNoOtherCalls();
        }
    }
}
