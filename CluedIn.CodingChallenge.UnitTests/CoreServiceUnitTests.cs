namespace CluedIn.CodingChallenge.UnitTests
{
    using CluedIn.CodingChallenge.Src.PubSub.Publishers;
    using CluedIn.CodingChallenge.Src.Services;
    using Moq;
    using Xunit;

    public class CoreServiceUnitTests
    {
        [Fact]
        public void Execute_WhenCalled_PublishesWithReversedName()
        {
            // Arrange
            var publisher = new Mock<IPublisher>();
            var coreService = new CoreService(publisher.Object);
            var message = "123";

            // Act
            coreService.Execute(message);

            // Assert
            publisher.Verify(x => x.Publish("321"), Times.Once);
            publisher.VerifyNoOtherCalls();
        }
    }
}
