namespace CluedIn.CodingChallenge.UnitTests
{
    using AutoFixture;
    using CluedIn.CodingChallenge.Src.Logging;
    using CluedIn.CodingChallenge.Src.PubSub.Subscribers;
    using Moq;
    using Redbus.Events;
    using Xunit;

    public class SubscriberOneUnitTests
    {
        [Fact]
        public void Handle_WhenCalled_LogsToConsole()
        {
            // Arrange
            var fixture = new Fixture();
            var logger = new Mock<ILogger>();
            var subscriber = new SubscriberOne(logger.Object);
            var message = fixture.Create<string>();
            var @event = new PayloadEvent<string>(message);

            // Act
            subscriber.Handle(@event);

            // Assert
            logger.Verify(x => x.Log($"SubscriberOne: {message}"), Times.Once);
            logger.VerifyNoOtherCalls();
        }
    }
}
